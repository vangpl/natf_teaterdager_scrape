//doc: https://github.com/website-scraper/node-website-scraper
//install: npm install website-scraper
//run: node scrape.js username password

var username = process.argv.slice(2,3)
var password = process.argv.slice(3);

const scrape = require('website-scraper');
const options = {
  urls: ['http://' + username + ':' + password + '@medlemmer.natf.no/td2005/'],
  urlFilter: (url) => url.startsWith('http://' + username + ':' + password + '@medlemmer.natf.no'), // Filter links to other websites
  directory: '/var/www/html/2005/',
  recursive: true,

};
 
// with async/await
//const result = await scrape(options);
 
// with promise
scrape(options).then((result) => {});
 
// or with callback
scrape(options, (error, result) => {});

